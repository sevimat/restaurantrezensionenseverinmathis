const express = require("express");
const cors = require("cors");
const fs = require("fs");
const app = express();

app.use(cors());
app.use(express.json());

const filePath = "db.json";

let restaurants = require("./db.json");

app.get("/restaurants", (req, res) => {
  res.json(restaurants);
});

app.post("/save-restaurant", (req, res) => {
  let lastId =
    restaurants.length > 0 ? Math.max(...restaurants.map((r) => r.id)) : 0;
  lastId++;
  const newRestaurant = {
    ...req.body,
    id: lastId,
    menupreis: parseInt(req.body.menupreis),
    weinpreis: parseInt(req.body.weinpreis),
    bewertung: parseInt(req.body.bewertung),
  };

  restaurants.push(newRestaurant);
  fs.writeFileSync(filePath, JSON.stringify(restaurants, null, 2), "utf8");
  res.send("Restaurant hinzugefügt und Datei aktualisiert");
});

app.post("/Bearbeitung-Bewertung", (req, res) => {
  const {
    id,
    bewertung,
    name,
    adresse,
    menuname,
    menupreis,
    weinname,
    weinpreis,
    kommentar,
  } = req.body;
  const restaurantIndex = restaurants.findIndex(
    (restaurant) => restaurant.id === id
  );

  if (restaurantIndex !== -1) {
    restaurants[restaurantIndex].bewertung = bewertung;
    restaurants[restaurantIndex].name = name;
    restaurants[restaurantIndex].adresse = adresse;
    restaurants[restaurantIndex].menuname = menuname;
    restaurants[restaurantIndex].menupreis = menupreis;
    restaurants[restaurantIndex].weinname = weinname;
    restaurants[restaurantIndex].weinpreis = weinpreis;
    restaurants[restaurantIndex].kommentar = kommentar;
    fs.writeFileSync(filePath, JSON.stringify(restaurants, null, 2), "utf8");
    res.send("Bewertung aktualisiert");
  } else {
    res.status(404).send("Restaurant nicht gefunden");
  }
});

app.delete("/LoeschenBewertung/:id", (req, res) => {
  const { id } = req.params;
  const restaurantIndex = restaurants.findIndex(
    (restaurant) => restaurant.id === id
  );

  if (restaurantIndex !== "-1") {
    restaurants.splice(restaurantIndex, 1);
    fs.writeFileSync(filePath, JSON.stringify(restaurants, null, 2));
    res.send("Bewertung gelöscht");
  } else {
    res.status(404).send("Restaurant nicht gefunden");
  }
});

app.get("/search", (req, res) => {
  const { weinpreis, menupreis, bewertung } = req.query;

  let filteredRestaurants = restaurants.filter((restaurant) => {
    return (
      (weinpreis ? restaurant.weinpreis >= weinpreis : true) &&
      (menupreis ? restaurant.menupreis <= menupreis : true)
    );
  });

  if (weinpreis) filteredRestaurants = filteredRestaurants.slice(0, 5);
  if (menupreis) filteredRestaurants = filteredRestaurants.slice(0, 4);
  if (bewertung) filteredRestaurants = filteredRestaurants.slice(0, 5);

  filteredRestaurants.sort((a, b) => b.bewertung - a.bewertung);

  const topFiveRatedRestaurants = filteredRestaurants.slice(0, 5);

  res.json(topFiveRatedRestaurants, filteredRestaurants);
});

app.get("/sort", (req, res) => {
  const restaurantsData = fs.readFileSync(filePath);
  const restaurants = JSON.parse(restaurantsData);

  restaurants.sort((a, b) => b.bewertung - a.bewertung);

  const bestRatedRestaurants = restaurants.slice(0, 5);

  res.json(bestRatedRestaurants);
});

app.listen(8000, () => {
  console.log("Server is running on port 8000.");
});
