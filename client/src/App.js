import Axios from "axios";
import React, { useState, useEffect } from "react";
import "./App.css";

// Definiere die baseURL für deine API-Anfragen
const baseURL = "http://localhost:8000";

function App() {
  const [RestaurantList, setRestaurantList] = useState([]);
  const [reloadData, setReloadData] = useState(false);
  const [editBewertung, setEditBewertung] = useState("");
  const [editname, setEditname] = useState("");
  const [editadresse, setEditadresse] = useState("");
  const [editmenuname, setEditmenuname] = useState("");
  const [editmenupreis, setEditmenupreis] = useState("");
  const [editweinname, setEditweinname] = useState("");
  const [editweinpreis, setEditweinpreis] = useState("");
  const [searchWeinPreis, setSearchWeinPreis] = useState("");
  const [searchMenuPreis, setSearchMenuPreis] = useState("");
  const [editkommentar, setEditkommentar] = useState("");
  const [selectedRestaurant, setSelectedRestaurant] = useState(null);

  useEffect(() => {
    Axios.get(`${baseURL}/restaurants`)
      .then((response) => {
        setRestaurantList(response.data);
      })
      .catch((error) => {});
  }, [reloadData]);

  const searchRestaurants = () => {
    Axios.get(`${baseURL}/search`, {
      params: {
        weinpreis: searchWeinPreis,
        menupreis: searchMenuPreis,
      },
    }).then((response) => {
      setRestaurantList(response.data);
    });
  };

  const bestefünfrestaurants = () => {
    Axios.get(`${baseURL}/sort`).then((response) => {
      setRestaurantList(response.data);
    });
  };

  const send = () => {
    const newName = document.getElementsByName("name")[0].value;
    const newAdresse = document.getElementsByName("adresse")[0].value;

    const restaurantExists = RestaurantList.some(
      (restaurant) =>
        restaurant.name === newName && restaurant.adresse === newAdresse
    );

    if (restaurantExists) {
      alert("Dieses Restaurant existiert bereits.");
      return;
    }

    const timestamp = new Date().toISOString();
    Axios.post(`${baseURL}/save-restaurant`, {
      name: newName,
      adresse: newAdresse,
      menuname: document.getElementsByName("menuname")[0].value,
      menupreis: document.getElementsByName("menupreis")[0].value,
      weinname: document.getElementsByName("weinname")[0].value,
      weinpreis: document.getElementsByName("weinpreis")[0].value,
      bewertung: document.getElementsByName("bewertung")[0].value,
      kommentar: document.getElementsByName("kommentar")[0].value,
      timestamp: timestamp,
    }).then(() => {
      setReloadData(!reloadData);
    });
  };

  const editReview = (restaurantId) => {
    Axios.post(`${baseURL}/Bearbeitung-Bewertung`, {
      id: restaurantId,
      bewertung: editBewertung,
      name: editname,
      adresse: editadresse,
      menuname: editmenuname,
      menupreis: editmenupreis,
      weinname: editweinname,
      weinpreis: editweinpreis,
      timestamp: new Date().toISOString(),
      kommentar: editkommentar,
    }).then(() => {
      setReloadData(!reloadData);
      setSelectedRestaurant(null);
    });
  };

  const löschebewertung = (restaurantId) => {
    Axios.delete(`${baseURL}/LoeschenBewertung/${restaurantId}`)
      .then(() => {
        const updatedRestaurants = RestaurantList.filter(
          (restaurant) => restaurant.id !== restaurantId
        );
        setRestaurantList(updatedRestaurants);
      })
      .catch((error) => {
        console.error("Error deleting review:", error);
        alert("Fehler beim Löschen der Bewertung");
      });
  };

  return (
    <div className="App">
      <h1>Restaurant Rezensionen</h1>
      <div>
        <input
          type="number"
          placeholder="Weinpreis"
          value={searchWeinPreis}
          onChange={(e) => setSearchWeinPreis(e.target.value)}
        />
        <input
          type="number"
          placeholder="Menupreis"
          value={searchMenuPreis}
          onChange={(e) => setSearchMenuPreis(e.target.value)}
        />
        <button
          type="number"
          value={searchMenuPreis}
          onClick={bestefünfrestaurants}
        >
          Sort
        </button>
        <button onClick={searchRestaurants}>Suchen</button>
      </div>
      <label>
        Name:
        <input type="text" name="name" placeholder="Name" />
      </label>
      <label>
        Adresse:
        <input type="text" name="adresse" placeholder="AdressePostleitzahl" />
      </label>
      <label>
        Menuname:
        <input type="text" name="menuname" />
      </label>
      <label>
        Menupreis:
        <input type="number" name="menupreis" />
      </label>
      <label>
        Weinname:
        <input type="text" name="weinname" />
      </label>
      <label>
        Weinpreis:
        <input type="number" name="weinpreis" />
      </label>
      <label>
        Bewertung:
        <input type="number" name="bewertung" />
      </label>
      <label>
        Kommentar:
        <input type="string" name="kommentar" />
      </label>
      <button onClick={send}>Absenden</button>

      <h2>Restaurants</h2>
      {RestaurantList.map((val, key) => {
        return (
          <div className="nebeneinander" id="Liste" key={key}>
            <h3>Name: {val.name}</h3>
            <h3>Adresse/Ort: {val.adresse}</h3>
            <h3>
              Billigstes Menu: {val.menuname}, {val.menupreis}€
            </h3>
            <h3>
              Teuerster Wein: {val.weinname}, {val.weinpreis}€
            </h3>
            <h3>Bewertung: {val.bewertung}/5</h3>
            <h3>Kommentar: {val.kommentar}</h3>
            <h5>Zeitpunkt der Veröffentlichung: {val.timestamp}</h5>
            <button
              onClick={() => {
                setSelectedRestaurant(val.id);
                setEditBewertung(val.bewertung);
              }}
            >
              Bewertung bearbeiten
            </button>
            <button onClick={() => löschebewertung(val.id)}>
              Bewertung löschen
            </button>
            {selectedRestaurant === val.id && (
              <div>
                <input
                  placeholder="Name"
                  type="string"
                  value={editname}
                  onChange={(e) => setEditname(e.target.value)}
                />
                <input
                  placeholder="Adresse"
                  type="string"
                  value={editadresse}
                  onChange={(e) => setEditadresse(e.target.value)}
                />
                <input
                  placeholder="Menuname"
                  type="string"
                  value={editmenuname}
                  onChange={(e) => setEditmenuname(e.target.value)}
                />
                <input
                  placeholder="Menupreis"
                  type="number"
                  value={editmenupreis}
                  onChange={(e) => setEditmenupreis(e.target.value)}
                />
                <input
                  placeholder="Weinname"
                  type="string"
                  value={editweinname}
                  onChange={(e) => setEditweinname(e.target.value)}
                />
                <input
                  placeholder="Weinpreis"
                  type="number"
                  value={editweinpreis}
                  onChange={(e) => setEditweinpreis(e.target.value)}
                />
                <input
                  placeholder="Bewertung"
                  type="number"
                  value={editBewertung}
                  onChange={(e) => setEditBewertung(e.target.value)}
                />
                <input
                  placeholder="Kommentar"
                  type="string"
                  value={editkommentar}
                  onChange={(e) => setEditkommentar(e.target.value)}
                />
                <button onClick={() => editReview(val.id)}>Speichern</button>
              </div>
            )}
          </div>
        );
      })}
    </div>
  );
}

export default App;
