import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import axios from "axios";

import App from "./App";

jest.mock("axios");

test("loads restaurants", async () => {
  const mockData = [
    {
      name: "test",
      adresse: "test",
      menuname: "test",
      menupreis: 4324,
      weinname: "test",
      weinpreis: 432,
      bewertung: 4,
      kommentar: "keinen",
      timestamp: "2024-01-09T10:07:43.076Z",
      id: 1,
    },
  ];

  axios.get.mockResolvedValue({ data: mockData });

  render(<App />);

  await waitFor(() =>
    expect(axios.get).toHaveBeenCalledWith("http://localhost:8000/restaurants")
  );
});

test("loads restaurants after search", async () => {
  const data = [
    {
      name: "test",
      adresse: "test",
      menuname: "test",
      menupreis: 4324,
      weinname: "test",
      weinpreis: 432,
      bewertung: 4,
      kommentar: "keinen",
      timestamp: "2024-01-09T10:07:43.076Z",
      id: 1,
    },
  ];
  axios.get.mockImplementation((url) => {
    switch (url) {
      case "http://localhost:8000/restaurants":
        return Promise.resolve({ data: [] });
      case "http://localhost:8000/search":
        return Promise.resolve({ data });
      default:
        return Promise.reject(new Error("not found"));
    }
  });

  const { getByPlaceholderText, getByText } = render(<App />);

  fireEvent.change(getByPlaceholderText("Weinpreis"), {
    target: { value: "500" },
  });
  fireEvent.change(getByPlaceholderText("Menupreis"), {
    target: { value: "1000" },
  });
  fireEvent.click(getByText("Suchen"));

  await waitFor(() => {
    expect(axios.get).toHaveBeenCalledTimes(3);
    expect(axios.get).toHaveBeenNthCalledWith(
      1,
      "http://localhost:8000/restaurants"
    );
    expect(axios.get).toHaveBeenNthCalledWith(
      2,
      "http://localhost:8000/restaurants"
    );
    expect(axios.get).toHaveBeenNthCalledWith(
      3,
      "http://localhost:8000/search",
      { params: { weinpreis: "500", menupreis: "1000" } }
    );
  });
});
test("adds a new restaurant", async () => {
  axios.post.mockResolvedValue({});
  axios.get.mockResolvedValue({ data: [] }); // Mock axios.get to return an empty array
  const { getByText, getByPlaceholderText } = render(<App />);

  fireEvent.change(getByPlaceholderText("Name"), {
    target: { value: "New Restaurant" },
  });
  fireEvent.change(getByPlaceholderText("AdressePostleitzahl"), {
    target: { value: "New Address" },
  });

  fireEvent.click(getByText("Absenden"));

  expect(axios.post).toHaveBeenCalledWith(
    "http://localhost:8000/save-restaurant",
    {
      name: "New Restaurant",
      adresse: "New Address",
      menuname: "",
      menupreis: "",
      weinname: "",
      weinpreis: "",
      bewertung: "",
      kommentar: "",
      timestamp: expect.any(String),
    }
  );
});

test("updates restaurant", async () => {
  const mockData = {
    bewertung: "",
    name: "New Restaurant",
    adresse: "New Address",
    menuname: "",
    menupreis: "",
    weinname: "",
    weinpreis: "",
    kommentar: "",
  };

  axios.post.mockResolvedValue({});

  await axios.post("http://localhost:8000/Bearbeitung-Bewertung", mockData);

  expect(axios.post.mock.calls[1][0]).toEqual(
    "http://localhost:8000/Bearbeitung-Bewertung"
  );
  expect(axios.post.mock.calls[0][1]).toEqual({
    id: mockData.id,
    bewertung: mockData.bewertung,
    name: mockData.name,
    adresse: mockData.adresse,
    menuname: mockData.menuname,
    menupreis: mockData.menupreis,
    weinname: mockData.weinname,
    weinpreis: mockData.weinpreis,
    timestamp: expect.any(String),
    kommentar: mockData.kommentar,
  });
});
it("deletes restaurant", async () => {
  const mockSetRestaurantList = jest.fn();
  const RestaurantList = [
    { id: 1, name: "Restaurant 1" },
    { id: 2, name: "Restaurant 2" },
  ];

  axios.delete.mockResolvedValueOnce();

  await axios.delete(`http://localhost:8000/LoeschenBewertung/1`);

  const updatedRestaurants = RestaurantList.filter(
    (restaurant) => restaurant.id !== 1
  );
  mockSetRestaurantList(updatedRestaurants);

  expect(axios.delete).toHaveBeenCalledWith(
    "http://localhost:8000/LoeschenBewertung/1"
  );
  expect(mockSetRestaurantList).toHaveBeenCalledWith([
    { id: 2, name: "Restaurant 2" },
  ]);
});
